<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="label.title"/></title>

    <c:url var="semanticCssLink" value="/lib/semantic-ui/semantic.min.css"/>
    <link rel="stylesheet" type="text/css" href="${semanticCssLink}">

    <c:url var="bootstrapCssLink" value="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapCssLink}">
</head>

<body>
<center>
    <h1>
        <a href="<c:url value = "/"/>"><spring:message code="label.appName"/></a>
    </h1>
    <a href="<c:url value="/logout"/>">
        <spring:message code="label.logout"/>
    </a>
</center>

<center>
    <a href="<c:url value = "/food" />">
        <spring:message code="label.availableFoodList"/>
    </a>
    |
    <a href="<c:url value = "/menu" />">
        <spring:message code="label.dailyMealMenu"/>
    </a>
</center>

<br><br>

<center>
    <form:errors path="foodCommand.*"/> <br><br>
</center>

<center>
    <div>
        <c:url var="addFoodLink" value="/food"/>
        <form:form method="POST"
                   action="${addFoodLink}" modelAttribute="foodCommand">
            <table>
                <tr>
                    <td><form:label path="name"><spring:message code="label.nameTxt"/></form:label></td>
                    <td><form:input path="name"/></td>
                    <td align="right"><input type="submit" value="Add Food"/></td>
                </tr>
            </table>
        </form:form>
    </div>
</center>

<br><br><br>

<center>
    <table border="1" cellpadding="10">
        <tr>
            <th><spring:message code="label.nameTxt"/></th>
            <th COLSPAN="2"><spring:message code="label.actionTxt"/></th>
        </tr>

        <c:forEach items="${foods}" var="currentFood">
            <tr>
                <td><c:out value="${currentFood.name}"/></td>
                <td>
                    <div>
                        <c:url var="updateFoodLink" value="/food/update"/>
                        <form:form method="PUT"
                                   action="${updateFoodLink}"
                                   modelAttribute="foodCommand">
                            <form:hidden path="id" value="${currentFood.id}"/>
                            <form:input path="name" placeholder="New Name"/>
                            <input type="submit" value="Update"/>
                        </form:form>
                    </div>
                </td>
                <td>
                    <div>
                        <c:url var="deleteFoodLink" value="/food/${currentFood.id}/delete"/>
                        <form action="${deleteFoodLink}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Delete">
                        </form>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
</center>


<c:url var="jqueryLink" value="/js/jquery-3.4.1.min.js"/>
<script src="${jqueryLink}"></script>

<c:url var="semanticJsLink" value="/lib/semantic-ui/semantic.min.js"/>
<script src="${semanticJsLink}"></script>

<c:url var="bootstrapJsLink" value="/js/bootstrap.min.js"/>
<script src="${bootstrapJsLink}"></script>
</body>
</html>
