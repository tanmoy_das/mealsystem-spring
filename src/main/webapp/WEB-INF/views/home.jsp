<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="label.title"/></title>

    <c:url var="semanticCssLink" value="/lib/semantic-ui/semantic.min.css"/>
    <link rel="stylesheet" type="text/css" href="${semanticCssLink}">

    <c:url var="bootstrapCssLink" value="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapCssLink}">
</head>
<body>
<center>
    <h1>
        <a href="<c:url value = "/"/>"><spring:message code="label.appName"/></a>
    </h1>
    <a href="<c:url value="/logout"/>">
        <spring:message code="label.logout"/>
    </a>
</center>

<center>
    <a href="<c:url value = "/food" />">
        <spring:message code="label.availableFoodList"/>
    </a>
    |
    <a href="<c:url value = "/menu" />">
        <spring:message code="label.dailyMealMenu"/>
    </a>
</center>

<c:url var="jqueryLink" value="/js/jquery-3.4.1.min.js"/>
<script src="${jqueryLink}"></script>

<c:url var="semanticJsLink" value="/lib/semantic-ui/semantic.min.js"/>
<script src="${semanticJsLink}"></script>

<c:url var="bootstrapJsLink" value="/js/bootstrap.min.js"/>
<script src="${bootstrapJsLink}"></script>
</body>
</html>
