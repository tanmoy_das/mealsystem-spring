<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="label.title"/></title>

    <c:url var="semanticCssLink" value="/lib/semantic-ui/semantic.min.css"/>
    <link rel="stylesheet" type="text/css" href="${semanticCssLink}">

    <c:url var="bootstrapCssLink" value="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapCssLink}">
</head>

<body>
<center>
    <h1>
        <a href="<c:url value = "/"/>"><spring:message code="label.appName"/></a>
    </h1>
    <a href="<c:url value="/logout"/>">
        <spring:message code="label.logout"/>
    </a>
</center>

<center>
    <a href="<c:url value = "/food" />">
        <spring:message code="label.availableFoodList"/>
    </a>
    |
    <a href="<c:url value = "/menu" />">
        <spring:message code="label.dailyMealMenu"/>
    </a>
</center>

<br><br><br>

<center>
    <center>
        <form:errors path="mealTimeCommand.*"/><br>
        <form:errors path="menuCommand.*"/><br>
        <form:errors path="menuItemCommand.*"/><br>
    </center>
</center>

<center>
    <div>
        <c:forEach items="${days}" var="currentDay">
            <br>
            <div>
                <div style="border: #0e566c; border-style: solid">
                    <h1 style="display: inline-block"><b><u><i>${currentDay.name.toUpperCase()}</i></u></b></h1>

                    <c:url var="addMealTimeLink" value="/addMealTime"/>
                    <form:form style="display: inline-block; margin: 20px"
                               action="${addMealTimeLink}" method="post"
                               modelAttribute="mealTimeCommand">
                        <form:hidden path="day" value="${currentDay.id}"/>
                        <form:input path="time" placeholder=" Meal Time"/>
                        <input type="submit" value="Add Meal Time">
                    </form:form>

                </div>

                <br>
                <div>
                    <c:forEach items="${currentDay.mealTimes}" var="currentMealTime">
                        <div>
                            <br>
                            <h2 style="display: inline-block">${currentMealTime.time}</h2>

                            <c:url var="deleteMealTimeLink" value="/deleteMealTime"/>
                            <form:form style="display: inline-block"
                                       action="${deleteMealTimeLink}" method="DELETE" modelAttribute="mealTimeCommand">
                                <form:hidden path="id" value="${currentMealTime.id}"/>
                                <input type="submit" value="X">
                            </form:form>


                            <c:url var="addMenuLink" value="/addMenu"/>
                            <form:form action="${addMenuLink}" method="POST"
                                       modelAttribute="menuCommand">
                                <form:hidden path="mealTime" value="${currentMealTime.id}"/>
                                <form:input path="name" placeholder="New Menu Name"/>
                                <input type="submit" value="Add Menu">
                            </form:form>
                        </div>

                        <br>

                        <c:forEach items="${currentMealTime.menus}" var="currentMenu">

                            <div style="display: inline-block; border: #502aa1 2px; border-style: dashed; margin: 5px; padding: 5px">
                                <div>${currentMenu.name}
                                    <c:url var="deleteMenuLink" value="/deleteMenu"/>
                                    <form:form cssStyle="display: inline-block"
                                               action="${deleteMenuLink}" method="DELETE"
                                               modelAttribute="menuCommand">
                                        <form:hidden path="id" value="${currentMenu.id}"/>
                                        <input type="submit" value="X">
                                    </form:form>
                                </div>

                                <div>
                                    <c:url var="addMenuItemLink" value="/addMenuItem"/>
                                    <form:form action="${addMenuItemLink}" method="post"
                                               modelAttribute="menuItemCommand">
                                        <form:hidden path="menu" value="${currentMenu.id}"/>
                                        <form:select path="food">
                                            <option selected disabled>Select Food</option>
                                            <c:forEach items="${foods}" var="currentFood">
                                                <form:option value="${currentFood.id}">${currentFood.name}</form:option>
                                            </c:forEach>
                                        </form:select>
                                        <form:input path="maximumAmount" placeholder="maximum amount"/>
                                        <input type="submit" value="Add Menu Item">
                                    </form:form>
                                </div>

                                <table border="1" cellpadding="10" style=" margin: 10px">


                                    <tr>
                                        <th>Item</th>
                                        <th>Maximum Amount</th>
                                        <th>Action</th>
                                    </tr>

                                    <c:forEach items="${currentMenu.menuItems}" var="currentMenuItem">
                                        <tr>
                                            <td>${currentMenuItem.food.name}</td>
                                            <td>${currentMenuItem.maximumAmount}</td>
                                            <td>
                                                <c:url var="updateMenuItemLink" value="/updateMenuItem"/>
                                                <form:form style="display: inline-block"
                                                           action="${updateMenuItemLink}"
                                                           method="PUT"
                                                           modelAttribute="menuItemCommand">
                                                    <form:hidden path="id" value="${currentMenuItem.id}"/>
                                                    <form:hidden path="food" value="${currentMenuItem.food.id}"/>
                                                    <form:hidden path="menu" value="${currentMenuItem.menu.id}"/>
                                                    <form:input placeholder="New Maximum Amount"
                                                                path="maximumAmount" size="18"/>
                                                    <input type="submit" value="Update">
                                                </form:form>

                                                <c:url var="deleteMenuItemLink" value="/deleteMenuItem"/>
                                                <form:form style="display: inline-block"
                                                           action="${deleteMenuItemLink}"
                                                           method="DELETE" modelAttribute="menuItemCommand">
                                                    <form:hidden path="id" value="${currentMenuItem.id}"/>
                                                    <input type="submit" value="Delete">
                                                </form:form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </c:forEach>
                        <br>
                    </c:forEach>
                </div>
            </div>
            <br><br>
        </c:forEach>
    </div>
</center>


<c:url var="jqueryLink" value="/js/jquery-3.4.1.min.js"/>
<script src="${jqueryLink}"></script>

<c:url var="semanticJsLink" value="/lib/semantic-ui/semantic.min.js"/>
<script src="${semanticJsLink}"></script>

<c:url var="bootstrapJsLink" value="/js/bootstrap.min.js"/>
<script src="${bootstrapJsLink}"></script>
</body>
</html>
