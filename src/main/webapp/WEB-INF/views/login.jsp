<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="label.title"/></title>

    <c:url var="semanticCssLink" value="/lib/semantic-ui/semantic.min.css"/>
    <link rel="stylesheet" type="text/css" href="${semanticCssLink}">

    <c:url var="bootstrapCssLink" value="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapCssLink}">
</head>
<body>
<div class="container">
    <h1><spring:message code="label.loginForm"/></h1>
    <c:url var="loginLink" value="/login"/>
    <form:form action="${loginLink}" method="post" modelAttribute="userCommand">
        <form:input type="email" path="email"/>
        <form:input type="password" path="password"/>
        <input type="submit" value="Login">
    </form:form>

    <div>
        <spring:message code="label.dontHaveAnAccount"/>
        <a href="<c:url value="/register"/>">
            <spring:message code="label.registerHere"/>
        </a>
    </div>
</div>

<c:url var="jqueryLink" value="/js/jquery-3.4.1.min.js"/>
<script src="${jqueryLink}"></script>

<c:url var="semanticJsLink" value="/lib/semantic-ui/semantic.min.js"/>
<script src="${semanticJsLink}"></script>

<c:url var="bootstrapJsLink" value="/js/bootstrap.min.js"/>
<script src="${bootstrapJsLink}"></script>
</body>
</html>
