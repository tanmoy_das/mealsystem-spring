<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="label.title"/></title>

    <c:url var="semanticCssLink" value="/lib/semantic-ui/semantic.min.css"/>
    <link rel="stylesheet" type="text/css" href="${semanticCssLink}">

    <c:url var="bootstrapCssLink" value="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapCssLink}">
</head>
<body>
<br><br>
<div class="container text-center">
    <div class="row">
        <div class="col">
            <h1><spring:message code="label.registerForm"/></h1>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col">
            <c:url var="registerLink" value="/register"/>
            <form:form action="${registerLink}" method="post" modelAttribute="userCommand">
                <form:input type="email" path="email"/>
                <form:input type="password" path="password"/>
                <input type="submit" value="Register">
            </form:form>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col">
            <div><spring:message code="label.alreadyHaveAnAccount"/>
                <a href="<c:url value="/login"/>">
                    <spring:message code="label.loginHere"/>
                </a>
            </div>
        </div>
    </div>
</div>

<c:url var="jqueryLink" value="/js/jquery-3.4.1.min.js"/>
<script src="${jqueryLink}"></script>

<c:url var="semanticJsLink" value="/lib/semantic-ui/semantic.min.js"/>
<script src="${semanticJsLink}"></script>

<c:url var="bootstrapJsLink" value="/js/bootstrap.min.js"/>
<script src="${bootstrapJsLink}"></script>
</body>
</html>
