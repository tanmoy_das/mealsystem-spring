package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.MenuItemDao;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.domain.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
@Transactional
@Service
public class FoodService {

    @Autowired
    private FoodDao foodDao;
    @Autowired
    private MenuDao menuDao;
    @Autowired
    private MenuItemDao menuItemDao;

    public List<Food> findAllFoods() {
        return foodDao.findAll();
    }

    public void deleteFood(String foodName) {
        Optional<Food> optionalFood = foodDao.findByName(foodName);
        if (!optionalFood.isPresent()) {
            System.err.println("Invalid Food Name");
        } else {
            Food food = optionalFood.get();
            foodDao.delete(food);
        }
    }

    public void addFood(String foodName) {
        Food food = new Food(foodName);
        foodDao.saveOrUpdate(food);
    }

    public void updateFood(int id, String name) {
        Optional<Food> optionalFood = foodDao.find(id);
        if (optionalFood.isPresent()) {
            Food food = optionalFood.get();
            food.setName(name);
            foodDao.saveOrUpdate(food);
        }
    }

    public void deleteFood(int id) {
        Optional<Food> optionalFood = foodDao.find(id);
        if (optionalFood.isPresent()) {
            Food food = optionalFood.get();

            food.getMenuItems().forEach(menuItem -> {
                Menu menu = menuItem.getMenu();
                menu.getMenuItems().remove(menuItem);
                menuDao.saveOrUpdate(menu);

                menuItemDao.delete(menuItem);
            });

            foodDao.delete(food);
        }
    }

    public Optional<Food> find(int id) {
        return foodDao.find(id);
    }

    public void saveOrUpdate(Food food) {
        foodDao.saveOrUpdate(food);
    }

    public boolean isPresent(int id) {
        Optional<Food> optionalFood = foodDao.find(id);
        return optionalFood.isPresent();
    }
}
