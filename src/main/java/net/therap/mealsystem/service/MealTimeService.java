package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.MenuItemDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.domain.MenuItem;
import net.therap.mealsystem.exception.DayNotValidException;
import net.therap.mealsystem.exception.MealTimeNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
@Transactional
@Service
public class MealTimeService {

    @Autowired
    private DayDao dayDao;
    @Autowired
    private MealTimeDao mealTimeDao;
    @Autowired
    private MenuDao menuDao;
    @Autowired
    private MenuItemDao menuItemDao;

    public void addMealTime(String dayName, String time) throws DayNotValidException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        if (optionalDay.isPresent()) {
            addMealTime(time, optionalDay.get());
        } else {
            throw new DayNotValidException();
        }
    }

    public void addMealTime(int id, String time) throws DayNotValidException {
        Optional<Day> optionalDay = dayDao.find(id);

        if (optionalDay.isPresent()) {
            addMealTime(time, optionalDay.get());
        } else {
            throw new DayNotValidException();
        }
    }

    private void addMealTime(String time, Day day) throws DayNotValidException {
        if (day == null) {
            throw new DayNotValidException();
        } else {
            MealTime mealTime = new MealTime(day, time);
            addMealTime(mealTime);
        }
    }

    public void deleteMealTime(String dayName, String time) {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        if (!optionalDay.isPresent()) {
            throw new DayNotValidException();
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.findByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                throw new MealTimeNotValidException();
            } else {
                MealTime mealTime = optionalMealTime.get();
                day.getMealTimes().remove(mealTime);

                List<Menu> menuList = mealTime.getMenus();
                for (Menu menu : menuList) {
                    for (MenuItem menuItem : menu.getMenuItems()) {
                        menuItemDao.delete(menuItem);
                    }
                    menuDao.delete(menu);
                }

                dayDao.saveOrUpdate(day);
                mealTimeDao.delete(mealTime);
            }
        }
    }

    public void deleteMealTime(MealTime mealTime) {
        mealTime = mealTimeDao.saveOrUpdate(mealTime);

        Day day = dayDao.saveOrUpdate(mealTime.getDay());
        day.getMealTimes().remove(mealTime);
        dayDao.saveOrUpdate(day);

        for (Menu menu : mealTime.getMenus()) {
            menu = menuDao.saveOrUpdate(menu);
            for (MenuItem menuItem : menu.getMenuItems()) {
                menuItem = menuItemDao.saveOrUpdate(menuItem);

                menuItemDao.delete(menuItem);
            }
            menuDao.delete(menu);
        }

        mealTimeDao.delete(mealTime);
    }

    public void deleteMealTime(int id) {
        Optional<MealTime> optionalMealTime = mealTimeDao.find(id);
        if (optionalMealTime.isPresent()) {
            MealTime mealTime = optionalMealTime.get();
            mealTimeDao.delete(mealTime);
        }
    }

    public void addMealTime(MealTime mealTime) {
        mealTimeDao.saveOrUpdate(mealTime);

        Day day = mealTime.getDay();
        day = dayDao.saveOrUpdate(day);

        day.getMealTimes().add(mealTime);
        dayDao.saveOrUpdate(day);

        Menu menu = new Menu();
        menu.setMealTime(mealTime);
        menu.setName(Menu.DEFAULT_NAME);
        menuDao.saveOrUpdate(menu);

        mealTime.getMenus().add(menu);
        mealTimeDao.saveOrUpdate(mealTime);


        mealTimeDao.saveOrUpdate(mealTime);
    }

    public Optional<MealTime> find(int id) {
        return mealTimeDao.find(id);
    }
}
