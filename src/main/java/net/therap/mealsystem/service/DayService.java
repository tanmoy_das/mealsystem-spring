package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.domain.*;
import net.therap.mealsystem.exception.DayNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
@Transactional
@Service
public class DayService {

    @Autowired
    private DayDao dayDao;

    public Day findDayByName(String dayName) throws DayNotValidException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        if (optionalDay.isPresent()) {
            return optionalDay.get();
        } else {
            throw new DayNotValidException();
        }
    }

    public void initializeDays() {
        String[] dayNames = Day.DAY_NAMES;

        List<Day> days = new ArrayList<>();
        for (int i = 0; i < dayNames.length; i++) {
            days.add(new Day(i + 1, dayNames[i]));
        }

        for (Day day : days) {
            dayDao.saveOrUpdate(day);
        }
    }

    public List<Day> findAllDaysWithDetails() {
        List<Day> days = dayDao.findAll();
        for (Day day : days) {

            List<MealTime> mealTimes = day.getMealTimes();
            for (MealTime mealTime : mealTimes) {
                List<Menu> menus = mealTime.getMenus();
                for (Menu menu : menus) {
                    List<MenuItem> menuItems = menu.getMenuItems();
                    for (MenuItem menuItem : menuItems) {
                        Food food = menuItem.getFood();
                    }
                }
            }
        }
        return days;
    }

    public Optional<Day> find(int id) {
        return dayDao.find(id);
    }
}
