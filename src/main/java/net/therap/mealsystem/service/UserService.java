package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.UserDao;
import net.therap.mealsystem.domain.Token;
import net.therap.mealsystem.domain.User;
import net.therap.mealsystem.util.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
@Transactional
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public Optional<User> getUserFromToken(String token) {
        if (userDao != null) {
            return userDao.findUserByToken(token);
        } else {
            return Optional.empty();
        }
    }

    public void registerUser(User user) {
        userDao.saveOrUpdate(user);
    }

    public void registerUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        registerUser(user);
    }

    public Optional<Token> loginUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        return loginUser(user);
    }

    public Optional<Token> loginUser(User user) {
        Optional<User> optionalUser = userDao.find(user);
        System.out.println(user);

        if (optionalUser.isPresent()) {
            user = optionalUser.get();

            RandomStringGenerator randomStringGenerator = new RandomStringGenerator();
            String tokenTxt = randomStringGenerator.getAlphaNumericString();

            Token token = new Token(tokenTxt);
            token.setUser(user);

            userDao.addToken(user, token);

            return Optional.of(token);
        }

        return Optional.empty();
    }

    public void invalidateToken(String tokenTxt) {
        Optional<User> optionalUser = getUserFromToken(tokenTxt);
        if (!optionalUser.isPresent()) {
            return;
        }

        User user = optionalUser.get();
        Optional<Token> optionalToken = user.getTokens()
                .stream()
                .filter(t -> t.getValue().equals(tokenTxt))
                .findAny();

        if (optionalToken.isPresent()) {
            Token token = optionalToken.get();
            user.getTokens().remove(token);

            userDao.deleteToken(user, token);
        }
    }
}
