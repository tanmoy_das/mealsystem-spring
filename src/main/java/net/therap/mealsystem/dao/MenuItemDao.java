package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.MenuItem;

import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/7/20
 */
public interface MenuItemDao {
    Optional<MenuItem> find(int id);

    List<MenuItem> findAll();

    Optional<MenuItem> save(MenuItem menuItem);

    void update(MenuItem menuItem);

    void delete(MenuItem menuItem);

    MenuItem saveOrUpdate(MenuItem menuItem);
}
