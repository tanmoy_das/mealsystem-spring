package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.exception.MealTimeNotValidException;

import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
public interface MenuDao {
    Optional<Menu> findFirst(MealTime mealTime) throws MealTimeNotValidException;

    List<Menu> findAll();

    List<Menu> findByDay(Day day);

    void save(Menu menu);

    void update(Menu menu);

    Menu saveOrUpdate(Menu menu);

    void delete(Menu menu);

    Optional<Menu> find(int id);

    Optional<Menu> find(Menu menu);
}
