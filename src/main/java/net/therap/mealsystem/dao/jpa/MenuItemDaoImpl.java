package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.domain.MenuItem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
@Repository
public class MenuItemDaoImpl implements net.therap.mealsystem.dao.MenuItemDao {

    @PersistenceContext(unitName = "mealsystem")
    private EntityManager em;

    @Override
    public Optional<MenuItem> find(int id) {
        MenuItem menuItem = em.find(MenuItem.class, id);

        return Optional.ofNullable(menuItem);
    }

    private Optional<Object> find(MenuItem menuItem) {
        TypedQuery<MenuItem> query = em.createQuery("FROM MenuItem WHERE menu = :menu AND food = :food", MenuItem.class);
        query.setParameter("menu", menuItem.getMenu());
        query.setParameter("food", menuItem.getFood());

        try {
            MenuItem menuItem1 = query.getSingleResult();
            return Optional.of(menuItem1);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<MenuItem> findAll() {
        TypedQuery<MenuItem> query = em.createQuery("FROM MenuItem ORDER BY id", MenuItem.class);

        return query.getResultList();
    }


    @Override
    @Transactional
    public Optional<MenuItem> save(MenuItem menuItem) {
        em.persist(menuItem);

        em.flush();
        return Optional.ofNullable(menuItem);
    }

    @Override
    @Transactional
    public void update(MenuItem menuItem) {
        em.merge(menuItem);

        em.flush();
    }

    @Override
    @Transactional
    public void delete(MenuItem menuItem) {
        em.remove(menuItem);

        em.flush();
    }

    @Override
    @Transactional
    public MenuItem saveOrUpdate(MenuItem menuItem) {
        MenuItem persistedMenuItem;

        if (menuItem.isNew()) {
            em.persist(menuItem);
            persistedMenuItem = menuItem;
        } else {
            persistedMenuItem = em.merge(menuItem);
        }

        em.flush();
        return persistedMenuItem;
    }
}
