package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.UserDao;
import net.therap.mealsystem.domain.Token;
import net.therap.mealsystem.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext(unitName = "mealsystem")
    private EntityManager em;

    @Override
    public Optional<User> findUserByToken(String tokenTxt) {
        String sql = "SELECT t FROM Token t WHERE t.value = :token";
        Query query = em.createQuery(sql);
        query.setParameter("token", tokenTxt);

        User user = null;
        try {
            Token token = (Token) query.getSingleResult();
            user = token.getUser();
        } catch (NoResultException ignored) {
        }

        return Optional.ofNullable(user);
    }

    @Override
    @Transactional
    public void save(User user) {
        em.persist(user);

        em.flush();
    }

    @Override
    public User find(int id) {
        return em.find(User.class, id);
    }

    @Override
    @Transactional
    public Optional<User> find(User u) {
        String sql = "SELECT u FROM User u WHERE u.email = :email AND u.password = :password";
        TypedQuery<User> query = em.createQuery(sql, User.class);

        query.setParameter("email", u.getEmail());
        query.setParameter("password", u.getPassword());

        List<User> resultList = query.getResultList();
        if (resultList.size() == 0) {
            return Optional.empty();
        } else {
            User user = resultList.get(0);
            return Optional.of(user);
        }
    }

    @Override
    @Transactional
    public void addToken(User user, Token token) {
        token.setUser(user);
        user.getTokens().add(token);

        em.merge(user);

        em.flush();
    }

    @Override
    @Transactional
    public void update(User user) {
        em.merge(user);

        em.flush();
    }

    @Override
    @Transactional
    public User saveOrUpdate(User user) {
        User persistedUser;

        if (user.isNew()) {
            em.persist(user);
            persistedUser = user;
        } else {
            persistedUser = em.merge(user);
        }

        em.flush();
        return persistedUser;
    }

    @Override
    @Transactional
    public void deleteToken(User user, Token token) {
        user.getTokens().remove(token);
        em.remove(token);
        em.merge(user);

        em.flush();
    }
}
