package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.domain.Day;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
@Component
public class DayDaoImpl implements DayDao {

    @PersistenceContext(unitName = "mealsystem")
    EntityManager em;

    @Override
    public Optional<Day> find(int id) {
        Day day = em.find(Day.class, id);

        return Optional.ofNullable(day);
    }

    public Optional<Day> find(Day day) {
        TypedQuery<Day> query = em.createQuery("FROM Day d WHERE name = :name", Day.class);
        query.setParameter("name", day.getName());

        try {

            Day savedDay = query.getSingleResult();
            return Optional.of(savedDay);

        } catch (NoResultException e) {
            return Optional.empty();
        }

    }

    @Override
    public List<Day> findAll() {
        TypedQuery<Day> query = em.createQuery("FROM Day ORDER BY id", Day.class);

        return query.getResultList();
    }

    @Override
    @Transactional
    public Day saveOrUpdate(Day day) {
        Day persistedDay;

        if (day.isNew()) {
            em.persist(day);
            persistedDay = day;
        } else {
            persistedDay = em.merge(day);
        }

        em.flush();
        return persistedDay;
    }

    @Override
    @Transactional
    public Optional<Day> save(Day day) {
        em.persist(day);

        em.flush();
        return Optional.of(day);
    }

    @Override
    public Optional<Day> findByName(String dayName) {
        TypedQuery<Day> query = em.createQuery("FROM Day WHERE name = :name", Day.class);
        query.setParameter("name", dayName);

        Day day;
        try {
            day = query.getSingleResult();
        } catch (NoResultException e) {
            day = null;
        }

        return Optional.ofNullable(day);
    }
}
