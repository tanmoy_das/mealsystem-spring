package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.exception.MealTimeNotValidException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
@Repository
public class MealTimeDaoImpl implements MealTimeDao {

    @PersistenceContext(unitName = "mealsystem")
    private EntityManager em;

    @Override
    public Optional<MealTime> find(int id) {
        MealTime mealTime = em.find(MealTime.class, id);

        return Optional.ofNullable(mealTime);
    }

    public Optional<MealTime> find(MealTime mealTime) {
        TypedQuery<MealTime> query = em.createQuery("FROM MealTime WHERE day = :day AND time = :time", MealTime.class);
        query.setParameter("day", mealTime.getDay());
        query.setParameter("time", mealTime.getTime());

        try {
            MealTime mealTime1 = query.getSingleResult();
            return Optional.of(mealTime1);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<MealTime> findAll() {
        TypedQuery<MealTime> query = em.createQuery("FROM MealTime ORDER BY id", MealTime.class);
        List<MealTime> resultList = query.getResultList();

        resultList.sort(Comparator.comparingInt(MealTime::getId));
        return resultList;
    }

    @Override
    @Transactional
    public MealTime saveOrUpdate(MealTime mealTime) {
        MealTime persistedMealTime;

        if (mealTime.isNew()) {
            em.persist(mealTime);
            persistedMealTime = mealTime;
        } else {
            persistedMealTime = em.merge(mealTime);
        }

        em.flush();
        return persistedMealTime;
    }

    @Override
    @Transactional
    public Optional<MealTime> save(MealTime mealTime) {
        em.persist(mealTime);
        em.flush();

        return Optional.of(mealTime);
    }

    @Override
    public Optional<MealTime> findByDayTime(Day day, String time) {
        TypedQuery<MealTime> query = em.createQuery("FROM MealTime mealTime WHERE mealTime.day = :day AND time = :time", MealTime.class);
        query.setParameter("day", day);
        query.setParameter("time", time);

        MealTime mealTime;
        try {
            mealTime = query.getSingleResult();
        } catch (NoResultException ignored) {
            mealTime = null;
        }

        return Optional.ofNullable(mealTime);
    }

    public List<MealTime> findByDay(Day day) {
        TypedQuery<MealTime> query = em.createQuery("FROM MealTime WHERE day = :day", MealTime.class);
        query.setParameter("day", day);

        return query.getResultList();
    }

    @Override
    @Transactional
    public void update(MealTime mealTime) {
        em.merge(mealTime);

        em.flush();
    }

    @Override
    @Transactional
    public void delete(MealTime mealTime) {
        Optional<MealTime> optionalMealTime = find(mealTime);
        if (optionalMealTime.isPresent()) {
            em.remove(optionalMealTime.get());
            em.flush();
        } else {
            throw new MealTimeNotValidException();
        }

    }
}
