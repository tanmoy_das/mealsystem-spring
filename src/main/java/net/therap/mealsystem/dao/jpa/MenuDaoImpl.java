package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.exception.MealTimeNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
@Repository
public class MenuDaoImpl implements MenuDao {

    @PersistenceContext(unitName = "mealsystem")
    private EntityManager em;
    @Autowired
    private MealTimeDao mealTimeDao;

    public Optional<Menu> find(int id) {
        Menu menu = em.find(Menu.class, id);

        return Optional.ofNullable(menu);
    }

    public Optional<Menu> find(Menu menu) {
        TypedQuery<Menu> query = em.createQuery("FROM Menu WHERE mealTime = :mealTime AND name = :name", Menu.class);
        query.setParameter("mealTime", menu.getMealTime());
        query.setParameter("name", menu.getName());

        try {
            Menu menu1 = query.getSingleResult();
            return Optional.of(menu1);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Menu> findFirst(MealTime mealTime) throws MealTimeNotValidException {
        Optional<MealTime> optionalMealTime = mealTimeDao.findByDayTime(mealTime.getDay(), mealTime.getTime());
        if (!optionalMealTime.isPresent()) {
            throw new MealTimeNotValidException();
        }

        mealTime = optionalMealTime.get();

        Query query = em.createQuery("FROM Menu WHERE mealTime = :mealTime");
        query.setParameter("mealTime", mealTime);

        Menu menu = (Menu) query.getSingleResult();

        return Optional.ofNullable(menu);
    }

    @Override
    public List<Menu> findAll() {
        TypedQuery<Menu> query = em.createQuery("FROM Menu ORDER BY id", Menu.class);

        return query.getResultList();
    }

    @Override
    public List<Menu> findByDay(Day day) {
        List<MealTime> mealTimes = mealTimeDao.findByDay(day);

        TypedQuery<Menu> query = em.createQuery("FROM Menu menu WHERE menu.mealTime IN (:mealTimes)", Menu.class);
        query.setParameter("mealTimes", mealTimes);

        return query.getResultList();
    }

    @Override
    @Transactional
    public Menu saveOrUpdate(Menu menu) {
        Menu persistedMenu;

        if (menu.isNew()) {
            em.persist(menu);
            persistedMenu = menu;
        } else {
            persistedMenu = em.merge(menu);
        }

        em.flush();
        return persistedMenu;
    }

    @Override
    @Transactional
    public void save(Menu menu) {
        em.persist(menu);

        em.flush();
    }

    @Override
    @Transactional
    public void update(Menu menu) {
        em.merge(menu);
        em.flush();
    }

    @Override
    @Transactional
    public void delete(Menu menu) {
        find(menu).ifPresent(menu1 -> em.remove(menu1));

        em.flush();
    }
}
