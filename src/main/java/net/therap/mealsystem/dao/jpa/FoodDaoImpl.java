package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.domain.Food;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
@Repository
public class FoodDaoImpl implements FoodDao {

    @PersistenceContext(unitName = "mealsystem")
    private EntityManager em;

    @Override
    public Optional<Food> find(int id) {
        Food food = em.find(Food.class, id);

        return Optional.ofNullable(food);
    }

    public Optional<Food> find(Food food) {
        TypedQuery<Food> query = em.createQuery("FROM Food f WHERE f.name = :name", Food.class);
        query.setParameter("name", food.getName());

        try {
            Food savedFood = query.getSingleResult();
            return Optional.of(savedFood);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Food> findAll() {
        TypedQuery<Food> query = em.createQuery("FROM Food", Food.class);

        return query.getResultList();
    }

    @Transactional
    public Food saveOrUpdate(Food food) {
        Food persistedFood;

        if (food.isNew()) {
            em.persist(food);
            persistedFood = food;
        } else {
            persistedFood = em.merge(food);
        }

        em.flush();
        return persistedFood;
    }

    @Override
    @Transactional
    public Optional<Food> save(Food food) {
        em.persist(food);

        em.flush();
        return Optional.of(food);
    }

    @Override
    public Optional<Food> findByName(String foodName) {
        TypedQuery<Food> query = em.createQuery("FROM Food WHERE name = :name", Food.class);
        query.setParameter("name", foodName);

        Food food = query.getSingleResult();

        return Optional.ofNullable(food);
    }

    @Override
    @Transactional
    public void update(Food food) {
        em.merge(food);
        em.flush();
    }

    @Override
    @Transactional
    public void delete(Food food) {
        Optional<Food> optionalFood = find(food);
        optionalFood.ifPresent(value -> em.remove(value));

        em.flush();
    }
}
