package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;

import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
public interface MealTimeDao {
    Optional<MealTime> find(int id);

    List<MealTime> findAll();

    Optional<MealTime> save(MealTime mealTime);

    MealTime saveOrUpdate(MealTime mealTime);

    Optional<MealTime> findByDayTime(Day day, String time);

    void update(MealTime mealTime);

    void delete(MealTime mealTime);

    List<MealTime> findByDay(Day day);
}
