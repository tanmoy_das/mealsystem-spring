package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;

import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public interface DayDao {
    Optional<Day> find(int id);

    List<Day> findAll();

    Day saveOrUpdate(Day day);

    Optional<Day> save(Day day);

    Optional<Day> findByName(String dayName);
}
