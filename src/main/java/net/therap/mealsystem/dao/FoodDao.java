package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Food;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
@Repository
public interface FoodDao {
    Optional<Food> find(int id);

    List<Food> findAll();

    Optional<Food> save(Food food);

    Food saveOrUpdate(Food food);

    Optional<Food> findByName(String name);

    void update(Food food);

    void delete(Food food);
}
