package net.therap.mealsystem.exception;

/**
 * @author tanmoy.das
 * @since 3/25/20
 */
public class DuplicateEntryException extends RuntimeException {
    public DuplicateEntryException() {
        super("Duplicate Entry Found");
    }
}
