package net.therap.mealsystem.exception;

/**
 * @author tanmoy.das
 * @since 3/24/20
 */
public class FoodNotValidException extends RuntimeException {

    public FoodNotValidException() {
        super("Food not in food list");
    }
}
