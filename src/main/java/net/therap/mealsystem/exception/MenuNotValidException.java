package net.therap.mealsystem.exception;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class MenuNotValidException extends RuntimeException {

    public MenuNotValidException() {
        super("Menu doesn't exist at specified meal time");
    }
}
