package net.therap.mealsystem.exception;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class DayNotValidException extends RuntimeException {
    public DayNotValidException() {
        super("Day name invalid");
    }
}
