package net.therap.mealsystem.propertyeditor;

import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.service.MealTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/2/20
 */
@Service
public class MealTimePropertyEditor extends PropertyEditorSupport {

    @Autowired
    private MealTimeService mealTimeService;

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        int id = Integer.parseInt(idTxt);

        Optional<MealTime> optionalMealTime = mealTimeService.find(id);

        if (optionalMealTime.isPresent()) {
            setValue(optionalMealTime.get());
        } else {
            setValue(null);
        }
    }
}
