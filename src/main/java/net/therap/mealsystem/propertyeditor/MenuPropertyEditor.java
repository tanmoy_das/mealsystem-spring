package net.therap.mealsystem.propertyeditor;

import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/2/20
 */
@Service
public class MenuPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private MenuService menuService;

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        int id = Integer.parseInt(idTxt);

        Optional<Menu> optionalMenu = menuService.findMenu(id);

        if (optionalMenu.isPresent()) {
            setValue(optionalMenu.get());
        } else {
            setValue(null);
        }
    }
}
