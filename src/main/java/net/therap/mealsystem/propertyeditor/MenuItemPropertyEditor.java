package net.therap.mealsystem.propertyeditor;

import net.therap.mealsystem.domain.MenuItem;
import net.therap.mealsystem.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/2/20
 */
@Service
public class MenuItemPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private MenuService menuService;

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        int id = Integer.parseInt(idTxt);

        Optional<MenuItem> optionalMenuItem = menuService.findMenuItem(id);

        if (optionalMenuItem.isPresent()) {
            setValue(optionalMenuItem.get());
        } else {
            setValue(null);
        }
    }
}
