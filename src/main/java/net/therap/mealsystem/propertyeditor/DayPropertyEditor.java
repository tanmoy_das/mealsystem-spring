package net.therap.mealsystem.propertyeditor;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.service.DayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/2/20
 */
@Service
public class DayPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private DayService dayService;

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        try {
            int id = Integer.parseInt(idTxt);

            Optional<Day> optionalDay = dayService.find(id);

            if (optionalDay.isPresent()) {
                setValue(optionalDay.get());
            } else {
                setValue(null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}