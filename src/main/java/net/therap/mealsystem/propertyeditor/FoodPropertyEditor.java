package net.therap.mealsystem.propertyeditor;

import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/2/20
 */
@Service
public class FoodPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private FoodService foodService;

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        int id = Integer.parseInt(idTxt);

        Optional<Food> optionalFood = foodService.find(id);

        if (optionalFood.isPresent()) {
            setValue(optionalFood.get());
        } else {
            setValue(null);
        }
    }
}
