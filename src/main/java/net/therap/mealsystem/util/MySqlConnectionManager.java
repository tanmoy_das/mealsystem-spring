package net.therap.mealsystem.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/5/20
 */
public class MySqlConnectionManager {

    public static Connection instantiateConnection(String mysqlHost, Integer mysqlPort, String mysqlDatabase,
                                                   String mysqlUser, String mysqlPassword) throws SQLException {
        String connectionString = "jdbc:mysql://{host}:{port}/{database}";
        connectionString = connectionString.replace("{host}", mysqlHost);
        connectionString = connectionString.replace("{port}", String.valueOf(mysqlPort));
        connectionString = connectionString.replace("{database}", mysqlDatabase);

        return DriverManager.getConnection(connectionString, mysqlUser, mysqlPassword);
    }

    public static Connection createConnection() throws SQLException {
        String mysqlHost = "127.0.0.1";
        Integer mysqlPort = 3306;
        String mysqlDatabase = "mealsystem";
        String mysqlUser = "tanmoy";
        String mysqlPassword = "";

        return instantiateConnection(mysqlHost, mysqlPort, mysqlDatabase, mysqlUser, mysqlPassword);
    }
}
