package net.therap.mealsystem.controller;

import net.therap.mealsystem.domain.Token;
import net.therap.mealsystem.domain.User;
import net.therap.mealsystem.service.UserService;
import net.therap.mealsystem.util.HashingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/8/20
 */
@Controller
public class AuthController {

    public static final String REGISTER_PAGE = "register";
    public static final String LOGIN_PAGE = "login";
    public static final String REDIRECT_LOGIN = "redirect:/login";
    public static final String REDIRECT_ROOT = "redirect:/";
    public static final String REDIRECT_REGISTER = "redirect:/register";
    private static final String USER_COMMAND = "userCommand";
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String sendRegisterForm(ModelMap model) {
        model.put(USER_COMMAND, new User());
        return REGISTER_PAGE;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String sendLoginForm(ModelMap model) {
        model.put(USER_COMMAND, new User());
        return LOGIN_PAGE;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String handleLogout(HttpServletRequest req, HttpServletResponse resp) {
        Optional<Cookie> cookieOptional = Arrays.stream(req.getCookies())
                .filter(c -> c.getName().equals("token"))
                .findAny();

        if (cookieOptional.isPresent()) {
            Cookie cookie = cookieOptional.get();

            String tokenTxt = cookie.getValue();
            userService.invalidateToken(tokenTxt);

            cookie.setMaxAge(0);
            resp.addCookie(cookie);
        }

        return REDIRECT_LOGIN;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String handleRegister(@Valid @ModelAttribute(USER_COMMAND) User user, Errors errors, ModelMap model) {
        if (errors.hasErrors()) {
            model.put(USER_COMMAND, user);
            return REGISTER_PAGE;
        }

        try {
            user.setPassword(HashingUtil.getHash(user.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        Optional<Token> optionalToken = userService.loginUser(user);
        if (!optionalToken.isPresent()) {
            userService.registerUser(user);
            return REDIRECT_ROOT;
        } else {
            return REDIRECT_REGISTER;
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String handleLogin(@Valid @ModelAttribute(USER_COMMAND) User user, Errors errors, ModelMap model, HttpServletResponse response) {
        if (errors.hasErrors()) {
            model.put(USER_COMMAND, user);
            return LOGIN_PAGE;
        }


        try {
            user.setPassword(HashingUtil.getHash(user.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        Optional<Token> optionalToken = userService.loginUser(user);

        if (optionalToken.isPresent()) {
            Cookie cookie = new Cookie("token", optionalToken.get().getValue());
            cookie.setMaxAge(30 * 24 * 60 * 60);
            response.addCookie(cookie);
        }

        return REDIRECT_ROOT;
    }
}
