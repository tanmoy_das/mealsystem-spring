package net.therap.mealsystem.controller;

import net.therap.mealsystem.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Locale;

/**
 * @author tanmoy.das
 * @since 3/31/20
 */
@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    private static final String REDIRECT_LOGIN = "redirect:/login";
    private static final String HOME_PAGE = "home";
    private static final String TOKEN_COOKIE = "token";

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Locale locale, HttpServletRequest request, HttpServletResponse response) {
        logger.info("Welcome home! The client locale is {}.", locale);

        Cookie[] cookies = request.getCookies();

        Cookie emptyTokenCookie = new Cookie(TOKEN_COOKIE, "");
        emptyTokenCookie.setMaxAge(-1);

        if (cookies == null) {
            response.addCookie(emptyTokenCookie);
            return REDIRECT_LOGIN;
        }

        Cookie tokenCookie = Arrays.stream(cookies)
                .filter(cookie -> cookie.getName().equals(TOKEN_COOKIE))
                .findAny()
                .orElse(null);

        if (tokenCookie == null) {
            response.addCookie(emptyTokenCookie);
            return REDIRECT_LOGIN;
        }

        if (!userService.getUserFromToken(tokenCookie.getValue()).isPresent()) {
            response.addCookie(emptyTokenCookie);
            return REDIRECT_LOGIN;
        } else {
            return HOME_PAGE;
        }
    }
}
