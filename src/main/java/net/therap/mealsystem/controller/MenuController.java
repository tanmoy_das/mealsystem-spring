package net.therap.mealsystem.controller;

import net.therap.mealsystem.domain.*;
import net.therap.mealsystem.exception.MealTimeNotValidException;
import net.therap.mealsystem.exception.MenuItemNotValidException;
import net.therap.mealsystem.exception.MenuNotValidException;
import net.therap.mealsystem.propertyeditor.*;
import net.therap.mealsystem.service.DayService;
import net.therap.mealsystem.service.FoodService;
import net.therap.mealsystem.service.MealTimeService;
import net.therap.mealsystem.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 4/2/20
 */
@Controller
public class MenuController {

    private static final String MENU_PAGE = "menu";
    private static final String REDIRECT_MENU = "redirect:/menu";
    private static final String DAYS_LIST = "days";
    private static final String FOODS_LIST = "foods";
    private static final String MEAL_TIME_COMMAND = "mealTimeCommand";
    private static final String MENU_COMMAND = "menuCommand";
    private static final String MENU_ITEM_COMMAND = "menuItemCommand";

    @Autowired
    private FoodService foodService;
    @Autowired
    private DayService dayService;
    @Autowired
    private MealTimeService mealTimeService;
    @Autowired
    private MenuService menuService;

    @Autowired
    private DayPropertyEditor dayPropertyEditor;
    @Autowired
    private FoodPropertyEditor foodPropertyEditor;
    @Autowired
    private MealTimePropertyEditor mealTimePropertyEditor;
    @Autowired
    private MenuPropertyEditor menuPropertyEditor;
    @Autowired
    private MenuItemPropertyEditor menuItemPropertyEditor;

    private static final MealTime EMPTY_MEAL_TIME = new MealTime();
    private static final Menu EMPTY_MENU = new Menu("");
    private static final MenuItem EMPTY_MENU_ITEM = new MenuItem(null, "");


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Day.class, dayPropertyEditor);
        binder.registerCustomEditor(Food.class, foodPropertyEditor);
        binder.registerCustomEditor(MealTime.class, mealTimePropertyEditor);
        binder.registerCustomEditor(Menu.class, menuPropertyEditor);
        binder.registerCustomEditor(MenuItem.class, menuItemPropertyEditor);
    }

    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public String show(ModelMap model) {
        setupRefData(model, EMPTY_MEAL_TIME, EMPTY_MENU, EMPTY_MENU_ITEM);
        return MENU_PAGE;
    }

    @RequestMapping(value = "/addMealTime", method = RequestMethod.POST)
    public String addMealTime(@Valid @ModelAttribute(MEAL_TIME_COMMAND) MealTime mealTime,
                              Errors errors, ModelMap model) {
        if (errors.hasErrors()) {
            setupRefData(model, mealTime, EMPTY_MENU, EMPTY_MENU_ITEM);
            return MENU_PAGE;
        }

        mealTimeService.addMealTime(mealTime);

        return REDIRECT_MENU;
    }

    @RequestMapping(value = "/addMenu", method = RequestMethod.POST)
    public String addMenu(@Valid @ModelAttribute(MENU_COMMAND) Menu menu,
                          Errors errors, ModelMap model) {
        if (errors.hasErrors()) {
            setupRefData(model, EMPTY_MEAL_TIME, menu, EMPTY_MENU_ITEM);
            return MENU_PAGE;
        }

        menuService.addMenu(menu);

        return REDIRECT_MENU;
    }

    @RequestMapping(value = {"/addMenuItem", "/updateMenuItem"}, method = RequestMethod.POST)
    public String addOrUpdateMenuItem(@Valid @ModelAttribute(MENU_ITEM_COMMAND) MenuItem menuItem,
                                      Errors errors, ModelMap model) {

        if (errors.hasErrors()) {
            setupRefData(model, EMPTY_MEAL_TIME, EMPTY_MENU, menuItem);
            return MENU_PAGE;
        }

        menuService.addOrUpdateMenuItem(menuItem);

        return REDIRECT_MENU;
    }

    @RequestMapping(value = "/deleteMenuItem", method = {RequestMethod.DELETE, RequestMethod.POST})
    public String deleteMenuItem(@RequestParam("id") int id) {
        Optional<MenuItem> optionalMenuItem = menuService.findMenuItem(id);

        if (optionalMenuItem.isPresent()) {
            menuService.deleteMenuItem(optionalMenuItem.get());
        } else {
            throw new MenuItemNotValidException();
        }

        return REDIRECT_MENU;
    }

    @RequestMapping(value = "/deleteMenu", method = {RequestMethod.DELETE, RequestMethod.POST})
    public String deleteMenu(@RequestParam("id") int id) {
        Optional<Menu> optionalMenu = menuService.findMenu(id);

        if (optionalMenu.isPresent()) {
            menuService.deleteMenu(optionalMenu.get());
        } else {
            throw new MenuNotValidException();
        }

        return REDIRECT_MENU;
    }

    @RequestMapping(value = "/deleteMealTime", method = {RequestMethod.DELETE, RequestMethod.POST})
    public String deleteMealTime(@RequestParam("id") int id) {
        Optional<MealTime> optionalMealTime = mealTimeService.find(id);

        if (optionalMealTime.isPresent()) {
            mealTimeService.deleteMealTime(optionalMealTime.get());
        } else {
            throw new MealTimeNotValidException();
        }

        return REDIRECT_MENU;
    }

    public void addDaysAndFoods(ModelMap model) {
        List<Day> days = dayService.findAllDaysWithDetails();
        model.put(DAYS_LIST, days);

        List<Food> foods = foodService.findAllFoods();
        model.put(FOODS_LIST, foods);
    }

    public void setupRefData(ModelMap model, MealTime mealTime, Menu menu, MenuItem menuItem) {
        addDaysAndFoods(model);
        model.put(MEAL_TIME_COMMAND, mealTime);
        model.put(MENU_COMMAND, menu);
        model.put(MENU_ITEM_COMMAND, menuItem);
    }
}
