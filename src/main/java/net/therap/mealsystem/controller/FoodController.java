package net.therap.mealsystem.controller;

import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.exception.FoodNotValidException;
import net.therap.mealsystem.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/31/20
 */
@Controller
public class FoodController {

    private static final String FOOD_PAGE = "food";
    private static final String REDIRECT_FOOD = "redirect:/food";
    private static final String FOODS_LIST = "foods";
    private static final String FOOD_COMMAND = "foodCommand";

    @Autowired
    private FoodService foodService;

    @RequestMapping(value = "/food", method = RequestMethod.GET)
    public String show(ModelMap model) {
        List<Food> foods = foodService.findAllFoods();
        Food food = new Food();

        model.put(FOODS_LIST, foods);
        model.put(FOOD_COMMAND, food);

        return FOOD_PAGE;
    }


    @RequestMapping(value = {"/food", "/food/update"}, method = RequestMethod.POST)
    public String createOrUpdate(@Valid @ModelAttribute(FOOD_COMMAND) Food food, Errors errors, ModelMap model) {
        if (errors.hasErrors()) {
            List<Food> foods = foodService.findAllFoods();

            model.put(FOODS_LIST, foods);
            model.put(FOOD_COMMAND, food);

            return FOOD_PAGE;
        }

        foodService.saveOrUpdate(food);

        return REDIRECT_FOOD;
    }

    @RequestMapping(value = "/food/{id}/delete", method = {RequestMethod.DELETE, RequestMethod.POST})
    public String delete(@PathVariable("id") int id) {
        if (!foodService.find(id).isPresent()) {
            throw new FoodNotValidException();
        }

        foodService.deleteFood(id);
        return REDIRECT_FOOD;
    }

}
