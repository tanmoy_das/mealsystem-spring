package net.therap.mealsystem.view;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.Menu;

import java.sql.SQLException;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/16/20
 */
public interface MenuViewer extends Viewer<Menu> {
    @Override
    void view(Menu item) throws SQLException;

    @Override
    void view(List<Menu> menus) throws SQLException;

    void view(List<Menu> menus, Day[] days);
}
