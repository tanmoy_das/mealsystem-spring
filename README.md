# Meal System for Therap (Assignment 3)

This is the project containing a simple meal management system for Therap BD as a part of Assignment 3 of Therap Training System.

Remember to migrate the first time you run this.

**Migration Commands:**

	# Migrate 
	gradle clean && gradle build && gradle -q run --args='--migrate'
	 
	# Rollback Migrations
	gradle clean && gradle -q run --args='--migrate_rollback'
	
	# Refresh Migrations:
	gradle clean && gradle -q run --args='--migrate_refresh'


***

**Food Management Commands:**

	# Add Food
	gradle clean && gradle build && gradle -q run --args='--addFood "vegetable curry"'
	gradle clean && gradle build && gradle -q run --args='--addFood chicken'
	gradle clean && gradle build && gradle -q run --args='--addFood "mutton"'
	gradle clean && gradle build && gradle -q run --args='--addFood "ice cream"'

	# Delete Food
	gradle clean && gradle build && gradle -q run --args='--deleteFood "vegetable curry"'
	gradle clean && gradle build && gradle -q run --args='--deleteFood "chicken"'

	# Show Foods
	gradle clean && gradle build && gradle -q run --args='--showFoods'

	# Show Foods (alternative)
	gradle clean && gradle build && gradle -q run --args='--viewFoods'

***

**MealTime Management Commands:**

    # Add Breakfast and Lunch for Every Day (Hibernated App only)
    gradle clean && gradle build && gradle -q run --args='--addBreakfastLunch'
    
    # Add New Meal Time
    gradle clean && gradle build && gradle -q run --args='--addMealTime --day saturday --time dinner'
    
    # Delete Meal Time
    gradle clean && gradle build && gradle -q run --args='--deleteMealTime --day saturday --time dinner'
***

**Menu Management Commands:**

    # Create Menu
    gradle clean && gradle build && gradle -q run --args='--addMenu --day saturday --time dinner --menuName "Alternate Menu"'
    
    # Delete Menu
    gradle clean && gradle build && gradle -q run --args='--deleteMenu --day saturday --time dinner --menuName "Alternate Menu"'
    
***

	# Add Item to Menu
	gradle clean && gradle build && gradle -q run --args='--addItem "vegetable curry" --day saturday --time lunch --maxAmount 1pc'
	gradle clean && gradle build && gradle -q run --args='--addItem "chicken" --day saturday --time lunch --maxAmount 3pc'
	
	gradle clean && gradle build && gradle -q run --args='--addItem "mutton" --day saturday --time breakfast --maxAmount 2pc'
	gradle clean && gradle build && gradle -q run --args='--addItem "ice cream" --day saturday --time breakfast --maxAmount "1 cone"'
	
	gradle clean && gradle build && gradle -q run --args='--addItem "ice cream" --day saturday --time dinner --maxAmount "1 cone" --menuName "Alternate Menu"'
	
	gradle clean && gradle build && gradle -q run --args='--addItem "ice cream" --day saturday --time dinner --maxAmount "1 cone" --menuName "general"'
	gradle clean && gradle build && gradle -q run --args='--addItem "chicken" --day saturday --time dinner --maxAmount "1 pc" --menuName "general"'
	
	# Update Item in Menu
	gradle clean && gradle build && gradle -q run --args='--updateItem "vegetable curry" --day saturday --time lunch --maxAmount unlimited'
	gradle clean && gradle build && gradle -q run --args='--updateItem "ice cream" --day saturday --time dinner --maxAmount "0.5 cone" --menuName "general"'
	
	# Remove Item from Menu
	gradle clean && gradle build && gradle -q run --args='--removeItem "ice cream" --day saturday --time breakfast'
	
	# Show Entire Menu 
	gradle clean && gradle build && gradle -q run --args='--viewMenu'
	
	# Show Menu by Day
	gradle clean && gradle build && gradle -q run --args='--viewMenu --day saturday'
	
	# Show Entire Menu (alternative)
	gradle clean && gradle build && gradle -q run --args='--showMenu'
	
	# Show Menu by Day (alternative)
	gradle clean && gradle build && gradle -q run --args='--showMenu --day saturday'
